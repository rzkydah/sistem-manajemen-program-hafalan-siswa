import { Component,OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LoginPage } from '../login/login.page';
import { RegisPage } from '../regis/regis.page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  constructor(
    public modalController : ModalController,
  ) {}

  ngOnInit() {

  }
  selectedSegment:any = 'home'

  async login()
  {
    const modal = await this.modalController.create({
      component: LoginPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }

  async regis()
  {
    const modal = await this.modalController.create({
      component: RegisPage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
