import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HomePage } from '../home/home.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  constructor(
    public modalController : ModalController,
  ) { }

  ngOnInit() {
  }

  selectedSegment:any = 'login'

  async home()
  {
    const modal = await this.modalController.create({
      component: HomePage,
      cssClass: 'my-custom-class'
    });
    return await modal.present();
  }
}
