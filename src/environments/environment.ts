// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyBWjL5dI9i3V6R15982hjmUxCwIB50w--0",
    authDomain: "manajemen-hafalan-siswa.firebaseapp.com",
    projectId: "manajemen-hafalan-siswa",
    storageBucket: "manajemen-hafalan-siswa.appspot.com",
    messagingSenderId: "451686921028",
    appId: "1:451686921028:web:db348a9c4c6c974e01dd11"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
